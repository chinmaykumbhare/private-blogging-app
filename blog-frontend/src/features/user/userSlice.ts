import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { UserSlice } from "../../components/DTO/userSlice.dto";

const initialState: UserSlice = {
  userData: {},
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUserData(state, action: PayloadAction<object>) {
      state.userData = action.payload;
    },
  },
});

export const { setUserData } = userSlice.actions;
export default userSlice.reducer;