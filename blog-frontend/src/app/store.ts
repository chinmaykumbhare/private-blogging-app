import { configureStore } from "@reduxjs/toolkit";
import userReducer from "../features/user/userSlice";
import blogsReducer from "../features/user/blogsSlice";

export const store = configureStore({
  reducer: {
    user: userReducer,
    blogs: blogsReducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
