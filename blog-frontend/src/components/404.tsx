import React from "react";
import { Row, Col, Card } from "react-bootstrap";

export default function Page404() {
  return (
      <Card>
          <Card.Img src="404.png" className="p-3"></Card.Img>
      </Card>
  );
}
