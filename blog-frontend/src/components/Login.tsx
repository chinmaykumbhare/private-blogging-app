import "./css/Login.css";
import { Button, Form, Nav } from "react-bootstrap";
import { useState } from "react";
import { LoginAPI } from "./API/Login.api";
import { useNavigate } from "react-router";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { VerifyTokenAPI } from "./API/VerifyToken.api";
import { setUserData } from "../features/user/userSlice";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const userData = useAppSelector(state => state.user.userData);
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  async function LoginHandler(): Promise<void> {
    const credentials = { username, password };
    const response = await LoginAPI(credentials);
    if (response.err) alert(response.err);
    else {
      localStorage.setItem("token", response.message.data);
      const getData =async (token:string | null) => {
        const data = await VerifyTokenAPI(token);
        const {username, email} = data.message.data;
        dispatch(setUserData({username, email}));
      }
      getData(response.message.data);
      navigate("/");
    }
  }

  return (
    <div className="container p-2 login">
      <h3 className="text-center">Login</h3>
      <Form>
        <Form.Group className="my-2">
          <Form.Control
            type="text"
            onChange={(event) => setUsername(event.target.value)}
            placeholder="username"
          ></Form.Control>
        </Form.Group>
        <Form.Group className="my-2">
          <Form.Control
            type="password"
            onChange={(event) => setPassword(event.target.value)}
            placeholder="password"
          ></Form.Control>
        </Form.Group>
        <Form.Group className="my-2 mx-1 text-center">
          <Button variant="success" onClick={() => LoginHandler()}>
            Login
          </Button>
          <Nav.Link href="/signup" className="text-decoration-underline">
            Signup Here
          </Nav.Link>
        </Form.Group>
      </Form>
    </div>
  );
}
