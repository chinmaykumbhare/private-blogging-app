import React, { useEffect, useState } from "react";
import "./css/BlogNavbar.css";
import { Container, Nav, Navbar } from "react-bootstrap";
import { useNavigate } from "react-router";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { setUserData } from "../features/user/userSlice";
import { VerifyTokenAPI } from "./API/VerifyToken.api";
import { setBlogData } from "../features/user/blogsSlice";

export default function BlogNavbar() {
  const [token, setToken] = useState<string | null>("");
  const userData = useAppSelector((state) => state.user.userData);
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  useEffect(() => {
    const _token: string | null = localStorage.getItem("token");
    setToken(_token);
    const getData = async (token: string | null) => {
      const data = await VerifyTokenAPI(token);
      if (data.message.data) {
        const { username, email } = data.message.data;
        dispatch(setUserData({ username, email }));
      }
    };
    getData(_token);
  }, []);

  function AccountHandler(): void {
    if (!token) navigate("/login");
    else navigate("/blogs");
  }

  function LogoutHandler(): void {
    localStorage.removeItem("token");
    dispatch(setUserData({}));
    dispatch(setBlogData({}));
    navigate("/login");
  }

  return (
    <div>
      <Navbar collapseOnSelect bg="dark" variant="dark" sticky="top" expand="sm">
        <Container>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse
            className="justify-content-end"
            id="responsive-navbar-nav"
          >
            <Navbar.Brand className="title" href="/blogs">
              BLOG
            </Navbar.Brand>
            <Nav className="me-auto my-auto">
              <Nav.Link href="/blogs">Home</Nav.Link>
              <Nav.Link href="/about">About</Nav.Link>
              <Nav.Link href="/blogs">Blogs</Nav.Link>
            </Nav>
            <Navbar.Text className="profile" onClick={() => AccountHandler()}>
              <i className="fas fa-user-circle lead"></i>
              <span className="mx-1 lead">
                {userData && Object(userData)["username"]}
              </span>
            </Navbar.Text>
            <Navbar.Text
              className="profile mx-3"
              onClick={() => LogoutHandler()}
            >
              <i className="fas fa-sign-out-alt title"></i>
            </Navbar.Text>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
