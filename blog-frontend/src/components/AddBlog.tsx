import React, { useEffect, useState } from "react";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "./css/AddBlog.css";
import { Form, Button } from "react-bootstrap";
import { AddPostAPI } from "./API/AddPost.api";
import { useNavigate } from "react-router";
import { useAppSelector } from "../app/hooks";

export default function AddBlog() {
  const [editorState, setEditorState] = useState<EditorState>(
    EditorState.createEmpty()
  );
  const [title, setTitle] = useState<string>("");
  const [body, setBody] = useState<string>("");
  const [file, setFile] = useState([]);
  const [tags, setTags] = useState<string>("");
  // safe fallback
  const [error, setError] = useState<string>("");

  const userData = useAppSelector((state) => state.user.userData);
  const navigate = useNavigate();

  useEffect(() => {
    if (!localStorage.getItem("token")) navigate("/login");
  }, []);

  async function PostHandler() {
    if (title === "" || body === ("<p></p>" || "") || tags === "") {
      setError("Please Fill Out All The Fields");
    } else {
      setError("");
      const token = localStorage.getItem("token");
      const postData = new FormData();
      postData.append("file", file as unknown as Blob);
      postData.append("token", token as unknown as Blob);
      postData.append("title", title);
      postData.append("body", body);
      postData.append("tags", tags);
      const status = await AddPostAPI(postData);
      if (status.err) alert(status.err);
      else {
        navigate("/blogs");
      }
    }
  }

  return (
    <div>
      <h3>Add a Blog!</h3>
      <Form className="add-blog">
        <Form.Group>
          <Form.FloatingLabel label="Title" className="pt-1">
            <Form.Control
              type="text"
              onChange={(event) => setTitle(event.target.value)}
            ></Form.Control>
          </Form.FloatingLabel>
        </Form.Group>
        {/* wysiwyg */}
        <Form.Group>
          <Form.Label>Body</Form.Label>
          <Editor
            editorState={editorState}
            wrapperClassName="card"
            editorClassName="card-body"
            onEditorStateChange={(newState) => {
              setEditorState(newState);
              setBody(draftToHtml(convertToRaw(newState.getCurrentContent())));
            }}
            toolbar={{
              options: [
                "inline",
                "blockType",
                "fontSize",
                "list",
                "textAlign",
                "history",
                "embedded",
                "emoji",
                "image",
              ],
              inline: { inDropdown: true },
              list: { inDropdown: true },
              textAlign: { inDropdown: true },
              link: { inDropdown: true },
              history: { inDropdown: true },
            }}
          />
        </Form.Group>
        <Form.Group className="my-2">
          <Form.Control
            type="file"
            accept="image/*"
            id="blog-pic"
            onChange={(event) => {
              // @ts-ignore
              let blogFile = event.target.files[0];
              setFile(blogFile);
            }}
          ></Form.Control>
        </Form.Group>
        <Form.Group className="my-2">
          <Form.FloatingLabel label="Tags" className="pt-1">
            <Form.Control
              type="text"
              onChange={(event) => setTags(event.target.value)}
            ></Form.Control>
          </Form.FloatingLabel>
        </Form.Group>
        <Form.Group>
          <Form.Label>{error}</Form.Label>
        </Form.Group>
        <Form.Group className="text-center">
          <Button onClick={PostHandler}>POST</Button>
        </Form.Group>
      </Form>
    </div>
  );
}
