import axios from "axios";
import { SignupURL } from "./BaseURLs";

export async function SignupAPI(credentials: object) {
    const status = await (await axios.post(SignupURL, credentials)).data;
    return status;
}