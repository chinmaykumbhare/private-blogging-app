import axios from "axios";
import { LoginURL } from "./BaseURLs";

export async function LoginAPI(credentials: object) {
    const token = await (await axios.post(LoginURL, credentials)).data;
    return token;
}