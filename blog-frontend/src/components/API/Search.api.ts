import axios from "axios";
import { SearchURL } from "./BaseURLs";

export async function SearchAPI(searchObj: object) {
    const searchData = await (await axios.post(SearchURL, searchObj)).data;
    return searchData;
}