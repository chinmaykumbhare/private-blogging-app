import axios from "axios";
import { AddPostURL } from "./BaseURLs";

export async function AddPostAPI(blogData: object) {
    const status = await (await axios.post(AddPostURL, blogData)).data;
    return status;
}