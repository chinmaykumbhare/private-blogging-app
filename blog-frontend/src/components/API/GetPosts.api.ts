import axios from "axios";
import { GetPostsURL } from "./BaseURLs";

export async function GetPostsAPI(token: string) {
    const posts = await (await axios.post(GetPostsURL, {token})).data;
    return posts;
}