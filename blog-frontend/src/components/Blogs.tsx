import React, { useEffect, useState } from "react";
import { Button, Card, CardGroup, Col, Row, Form, FormControl } from "react-bootstrap";
import { useNavigate } from "react-router";
import { useAppSelector, useAppDispatch } from "../app/hooks";
import { setBlogData } from "../features/user/blogsSlice";
import { GetPostsAPI } from "./API/GetPosts.api";
import { SearchAPI } from "./API/Search.api";
import "./css/Blogs.css";

export default function Blogs() {
  const [blogs, setBlogs] = useState<object[]>([]);
  const token = localStorage.getItem("token");

  const userData = useAppSelector((state) => state.user.userData);
  const blogsData = useAppSelector((state) => state.blogs.blogData);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  //useEffect for posts
  useEffect(() => {
    const getBlogs = async (token: string) => {
      const blogs = await GetPostsAPI(token);
      const { data } = blogs.message;
      dispatch(setBlogData({ ...data }));
    };
    const token: string | null = localStorage.getItem("token");
    token && getBlogs(token);
  }, []);

  useEffect(() => {
    setBlogs(Object.values(blogsData));
  }, [blogsData]);

  async function searchHandler(search: string) {
    const searchData = await SearchAPI({token, search});
    const { data } = searchData.message;
    dispatch(setBlogData({ ...data }));
  }

  return (
    <div>
      <Row className="blogs">
        <Col lg={3} className="border-dark border-end">
          <h3>Your Blogs</h3>
          { token && (<Form className="my-3">
            <FormControl
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
              onChange={event => {
                searchHandler(event.target.value);
              }}
            />
            <Form.Label className="w-100"><hr/><h5 className="text-center">OR</h5><hr/></Form.Label>
          </Form>)}
          <Button className="w-100" onClick={() => navigate("/addblog")}>
            ADD
          </Button>
        </Col>
        <Col lg={9}>
          {blogs.length ? (
            blogs.map((blog, index) => {
              const postBody = (blog as any).post[0].body;
              return (
                <Card key={index} className="mx-auto w-75 my-3">
                  <Card.Title className="m-2 border-bottom">
                    <h3>{(blog as any).post[0].title}</h3>
                  </Card.Title>
                  <Card.Text
                    className="m-2"
                    dangerouslySetInnerHTML={{ __html: postBody }}
                  ></Card.Text>
                  <Card.Img
                    src={"http://localhost:8090/blogs/" + (blog as any).image}
                    alt="cover"
                  ></Card.Img>
                </Card>
              );
            })
          ) : (
            <div style={{ textAlign: "center", marginTop: "17.5rem" }}>
              <h3>Feels a bit lonely here. Why not add a blog?</h3>
            </div>
          )}
        </Col>
      </Row>
    </div>
  );
}
