import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { BlogsSlice } from "../../components/DTO/blogsSlice.dto";

const initialState: BlogsSlice = {
  blogData: {},
};

const blogsSlice = createSlice({
  name: "blogs",
  initialState,
  reducers: {
    setBlogData(state, action: PayloadAction<object>) {
      state.blogData = action.payload;
    },
  },
});

export const { setBlogData } = blogsSlice.actions;
export default blogsSlice.reducer;