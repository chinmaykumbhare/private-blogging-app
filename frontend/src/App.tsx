import { Outlet } from "react-router";
import "./App.css";
import BlogNavbar from "./components/BlogNavbar";

function App() {
  return (
    <div>
      <div>
        <BlogNavbar />
        <div className="container my-3">
          <Outlet />
        </div>
      </div>
    </div>
  );
}

export default App;
