import axios from "axios";
import { VerifyTokenURL } from "./BaseURLs";

export async function VerifyTokenAPI(token: string | null) {
    const data = await (await axios.post(VerifyTokenURL, {token})).data;
    return data;
}