export const LoginURL = "http://localhost:8090/api/v1.0.1/user/verify";
export const SignupURL = "http://localhost:8090/api/v1.0.1/user/add";
export const VerifyTokenURL = "http://localhost:8090/api/v1.0.1/user/token";
export const GetPostsURL = "http://localhost:8090/api/v1.0.1/user/posts";