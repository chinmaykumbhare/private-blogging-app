import { BrowserRouter, Route, Routes } from "react-router-dom";
import App from "../App";
import About from "./About";
import AddBlog from "./AddBlog";
import Blogs from "./Blogs";
import Home from "./Home";
import Login from "./Login";
import Signup from "./Signup";

export default function BlogRoutes() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<App />}>
            <Route path="/" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/blogs" element={<Blogs />} />
            <Route path="/addblog" element={<AddBlog />} />
            <Route path="/login" element={<Login />} />
            <Route path="/signup" element={<Signup />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}
