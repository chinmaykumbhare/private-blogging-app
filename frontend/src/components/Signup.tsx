import "./Signup.css";
import { Button, Form, Nav } from "react-bootstrap";
import { useState } from "react";
import { LoginAPI } from "./API/Login.api";
import { useNavigate } from "react-router";
import { SignupAPI } from "./API/Signup.api";

export default function Signup() {
  const [firstname, setFirstName] = useState("");
  const [lastname, setLastName] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [cpassword, setCPassword] = useState("");
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const navigate = useNavigate();

  async function SignupHandler(): Promise<void> {
    const credentials = { firstname, lastname, email, username, password };
    const response = await SignupAPI(credentials);
    if (response.err) {
      setError(true);
      if(response.err === "MongoBulkWriteError") setErrorMessage("username already taken!");
      if(response.err == "ValidationError") setErrorMessage("please fill all fields!");
    }
    else {
      navigate("/login");
    }
  }

  return (
    <div className="container w-25 p-2 login">
      <h3 className="text-center">Signup</h3>
      <Form>
        <Form.Group className="my-2">
          <Form.Control
            type="text"
            onChange={(event) => setFirstName(event.target.value)}
            placeholder="first name"
          ></Form.Control>
        </Form.Group>
        <Form.Group className="my-2">
          <Form.Control
            type="text"
            onChange={(event) => setLastName(event.target.value)}
            placeholder="last name"
          ></Form.Control>
        </Form.Group>
        <Form.Group className="my-2">
          <Form.Control
            type="text"
            onChange={(event) => setEmail(event.target.value)}
            placeholder="email"
          ></Form.Control>
        </Form.Group>
        <Form.Group className="my-2">
          <Form.Control
            type="text"
            onChange={(event) => setUsername(event.target.value)}
            placeholder="username"
          ></Form.Control>
        </Form.Group>
        <Form.Group className="my-2">
          <Form.Control
            type="password"
            onChange={(event) => setPassword(event.target.value)}
            placeholder="password"
          ></Form.Control>
        </Form.Group>
        <Form.Group className="my-2">
          <Form.Control
            type="password"
            onChange={(event) => setCPassword(event.target.value)}
            placeholder="confirm password"
          ></Form.Control>
        </Form.Group>
        { error && (<Form.Group>
          <Form.Text>{errorMessage}</Form.Text>
        </Form.Group>)}
        <Form.Group className="my-2 mx-1 text-center">
          {password !== "" && password === cpassword && (
            <Button variant="success" onClick={() => SignupHandler()}>
              Register
            </Button>
          )}
          <Nav.Link href="/login" className="text-decoration-underline">
            Login Here
          </Nav.Link>
        </Form.Group>
      </Form>
    </div>
  );
}
