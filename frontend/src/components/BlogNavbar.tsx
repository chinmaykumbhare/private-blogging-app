import React, { useEffect, useState } from "react";
import "./BlogNavbar.css";
import { Container, Nav, Navbar } from "react-bootstrap";
import { useNavigate } from "react-router";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { setUserData } from "../features/user/userSlice";
import { VerifyTokenAPI } from "./API/VerifyToken.api";

export default function BlogNavbar() {
  const [token, setToken] = useState<string | null>("");
  const userData = useAppSelector(state => state.user.userData);
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  useEffect(() => {
    const _token: string | null = localStorage.getItem("token");
    setToken(_token);
    const getData =async (token:string | null) => {
      const data = await VerifyTokenAPI(token);
      const {username, email} = data.message.data;
      dispatch(setUserData({username, email}));
    }
    getData(_token);
  }, []);

  function AccountHandler(): void {
    if (!token) navigate("/login");
    else navigate("/blogs");
  }

  function LogoutHandler(): void {
    localStorage.removeItem("token");
    dispatch(setUserData({}));
    navigate("/login");
  }

  return (
    <div>
      <Navbar bg="dark" variant="dark" sticky="top" expand="md">
        <Container>
          <Navbar.Brand className="title" href="/">
            BLOG
          </Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
            <Nav.Link href="/blogs">Blogs</Nav.Link>
          </Nav>
          <Navbar.Collapse className="justify-content-end">
            <Navbar.Text className="profile" onClick={() => AccountHandler()}>
              <i className="fas fa-user-circle lead"></i>
              <span className="mx-1 lead">{Object(userData)["username"]}</span>
            </Navbar.Text>
            <Navbar.Text className="profile mx-3" onClick={() => LogoutHandler()}>
              <i className="fas fa-sign-out-alt title"></i>
            </Navbar.Text>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
