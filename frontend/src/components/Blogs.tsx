import React, { useEffect, useState } from "react";
import { Button, Card, CardGroup, Col, Row } from "react-bootstrap";
import { useNavigate } from "react-router";
import { useAppSelector, useAppDispatch } from "../app/hooks";
import { setBlogData } from "../features/user/blogsSlice";
import { GetPostsAPI } from "./API/GetPosts.api";
import "./Blogs.css";

export default function Blogs() {
  const [blogs, setBlogs] = useState<object[]>([]);

  const userData = useAppSelector((state) => state.user.userData);
  const blogsData = useAppSelector((state) => state.blogs.blogData);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  //useEffect for posts
  useEffect(() => {
    const getBlogs = async (token: string) => {
      const blogs = await GetPostsAPI(token);
      const { data } = blogs.message;
      // console.log(data);
      dispatch(setBlogData({ ...data }));
    };
    const token: string | null = localStorage.getItem("token");
    token && getBlogs(token);
  }, []);

  useEffect(() => {
    setBlogs(Object.values(blogsData));
  }, [blogsData]);

  // console.log(blogs);

  return (
    <div>
      <Row className="blogs">
        <Col lg={3} className="border-dark border-end">
          <h3>Your Blogs</h3>
          <Button className="w-100" onClick={() => navigate("/addblog")}>ADD</Button>
        </Col>
        <Col lg={9}>
          {blogs.length ? (
            blogs.map((blog, index) => {
              return (
                <Card key={index} className="mx-auto w-75 border">
                  <Card.Title className="m-2">{blog.post[0]}</Card.Title>
                </Card>
              );
            })
          ) : (
            <div style={{textAlign: "center", marginTop: "17.5rem"}}>
              <h3>Feels a bit lonely here. Why not add a blog?</h3>
            </div>
          )}
        </Col>
      </Row>
    </div>
  );
}
