import React from "react";
import "./AddBlog.css";
import { Form } from "react-bootstrap";
import BlogBodyEditor from "./BlogBodyEditor";

export default function AddBlog() {
  return (
    <div>
      <h3>Add a Blog!</h3>
      <Form className="w-75">
        <Form.Group>
          <Form.FloatingLabel label="Title" className="pt-1">
            <Form.Control type="text"></Form.Control>
          </Form.FloatingLabel>
        </Form.Group>
        {/* wysiwyg goes here */}
        <Form.Group>
          <Form.Label>Body</Form.Label>
          <BlogBodyEditor />
        </Form.Group>
        <Form.Group>
          <Form.FloatingLabel label="Tags" className="pt-1">
            <Form.Control type="text"></Form.Control>
          </Form.FloatingLabel>
        </Form.Group>
      </Form>
    </div>
  );
}
