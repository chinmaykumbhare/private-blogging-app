import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import BlogRoutes from "./components/BlogRoutes";
import { Provider } from "react-redux";
import { store } from "./app/store";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BlogRoutes />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
