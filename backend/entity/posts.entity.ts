import { model, Schema, Types, SchemaDefinitionProperty } from "mongoose";
import Posts from "../DTO/posts.dto";

const postsSchema = new Schema<Posts>({
    user: {
        type: Types.ObjectId,
        ref: "users",
        required: true
    },
    image: {
        type: String,
        required: true
    },
    post: {
        type: Array,
        required: true
    }
})

export default model("posts", postsSchema);