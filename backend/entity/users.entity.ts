import { model, Schema } from "mongoose";
import Users from "../DTO/user.dto";

const userSchema = new Schema<Users>({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    username: {
        type: String,
        unique: true,
        sparse: true,
        index: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    }
});

export default model<Users>("users", userSchema);