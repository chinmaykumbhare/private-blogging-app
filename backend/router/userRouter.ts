import { Router, Request, Response } from "express";
import { compare, hash } from "bcrypt";
import { sign } from "jsonwebtoken";
import usersEntity from "../entity/users.entity";
import { ResponseFormat } from "../modules/response";
import { postsRouter } from "./postsRouter";
import { profileRouter } from "./profileRouter";
import { Error } from "mongoose";
import { tokenRouter } from "./tokenRouter";
import { FirstNameValidator } from "../validations/firstname.validator";
import { LastNameValidator } from "../validations/lastname.validator";
import { UsernameValidator } from "../validations/username.validator";
import { PasswordValidator } from "../validations/password.validator";
import { EmailValidator } from "../validations/email.validator";

const router = Router();

router.get("/", async (request: Request, response: Response) => {
  const data = await usersEntity.find({});
  response.send(ResponseFormat("", { data: data }));
});

router.post(
  "/add",
  FirstNameValidator,
  LastNameValidator,
  UsernameValidator,
  PasswordValidator,
  EmailValidator,
  async (request: Request, response: Response) => {
    const { firstname, lastname, username, password, email } = request.body;
    hash(password, 10, async (err, hash) => {
      try {
        const data = await usersEntity.insertMany({
          username: username,
          firstname: firstname,
          lastname: lastname,
          password: hash,
          email: email,
        });
        response.send(ResponseFormat("", { data: data }));
      } catch (error: any | Error) {
        response.send(ResponseFormat(error.name, {}));
      }
    });
  }
);

router.post(
  "/verify",
  UsernameValidator,
  PasswordValidator,
  async (request: Request, response: Response) => {
    const { username, password } = request.body;
    const userData = await usersEntity.find({ username: username });
    if (!userData.length)
      response.send(ResponseFormat("Invalid username / password", {}));
    else {
      compare(password, userData[0].password, async (err, result) => {
        if (err) throw err;
        if (result) {
          const token = await sign(
            {
              _id: userData[0]._id,
              username: userData[0].username,
              email: userData[0].email,
            },
            process.env.PASSWORD_HASHED_KEY || "",
            { expiresIn: "3d" }
          );
          response.send(ResponseFormat("", { data: token }));
        }
        if (!result) {
          response.send(ResponseFormat("Invalid username / password", {}));
        }
      });
    }
  }
);

router.use("/posts", postsRouter);
router.use("/profile", profileRouter);
router.use("/token", tokenRouter);

export { router as userRouter };
