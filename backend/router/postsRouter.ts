import { Request, Response, Router } from "express";
import fs from "fs";
import multer from "multer";
import { verify } from "jsonwebtoken";
import postsEntity from "../entity/posts.entity";
import { ResponseFormat } from "../modules/response";
import { searchRouter } from "./searchRouter";
import { TokenValidator } from "../validations/token.validator";
import { TitleValidator } from "../validations/title.validator";
import { BodyValidator } from "../validations/body.validator";
import { TagsValidator } from "../validations/tags.validator";

const router = Router();

/**
 * post format:
 * {
 *      title: "",
 *      body: "",
 *      tags: ""
 * }
 */

router.post(
  "/add",
  multer().single("file"),
  TokenValidator,
  TitleValidator,
  BodyValidator,
  TagsValidator,
  async (request: Request, response: Response) => {
    const file = request.file;
    const { token, title, body, tags } = request.body;
    verify(
      token,
      process.env.PASSWORD_HASHED_KEY || "",
      async (err: any, decoded: any) => {
        if (err) response.send(ResponseFormat(err, {}));
        else {
          const date = new Date().getTime();
          // @path
          // E:\\Assignments\\personal-blog\\backend\\images\\blogs\\
          const fileName =
            process.cwd() +"\\images\\blogs\\" +
            date +
            "." +
            file?.originalname.split(".")[1];
          console.log(fileName);
          //   @ts-ignore
          fs.writeFile(fileName, file?.buffer ?? "", (fserror) => {
            if (fserror) {
              response.send(ResponseFormat(fserror.message, {}));
            }
          });
          const fileNameOnDB = fileName.split("blogs\\")[1];
          const data = await postsEntity.insertMany({
            user: decoded._id,
            image: fileNameOnDB,
            post: { title, body, tags },
          });
          response.send(ResponseFormat("", { data }));
        }
      }
    );
  }
);

router.post("/", async (request: Request, response: Response) => {
  const { token } = request.body;
  verify(
    token,
    process.env.PASSWORD_HASHED_KEY || "",
    async (err: any, decoded: any) => {
      if (err) throw err;
      const data = await postsEntity.find({
        user: decoded._id,
      });
      response.send(ResponseFormat("", { data: data }));
    }
  );
});

router.put("/edit", async (request: Request, response: Response) => {
  const { token, post } = request.body;
  if (!token) response.send(ResponseFormat("invalid token", {}));
  else {
    verify(
      token,
      process.env.PASSWORD_HASHED_KEY || "",
      async (err: any, decoded: any) => {
        if (err) throw err;
        const data = await postsEntity.updateOne(
          { user: decoded._id },
          {
            post: post,
          }
        );
        response.send(ResponseFormat("", { data: data }));
      }
    );
  }
});

router.use("/search", searchRouter);

export { router as postsRouter };
