import { Router, Request, Response } from "express";
import { verify } from "jsonwebtoken";
import { ResponseFormat } from "../modules/response";
import { TokenValidator } from "../validations/token.validator";

const router = Router();

router.post(
  "/",
  TokenValidator,
  async (request: Request, response: Response) => {
    const { token } = request.body;
    if (!token) response.send(ResponseFormat("TOKEN NOT PROVIDED", {}));
    else {
      verify(
        token,
        process.env.PASSWORD_HASHED_KEY || "",
        (err: any, data: any) => {
          if (err) response.send(ResponseFormat(err.name, {}));
          else response.send(ResponseFormat("", { data }));
        }
      );
    }
  }
);

export { router as tokenRouter };
