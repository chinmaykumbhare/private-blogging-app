import { Router, Request, Response } from "express";
import { verify } from "jsonwebtoken";
import postsEntity from "../entity/posts.entity";
import { ResponseFormat } from "../modules/response";

const router = Router();

router.post("/", async (request: Request, response: Response) => {
  const { search, token } = request.body;
  verify(
    token,
    process.env.PASSWORD_HASHED_KEY || "",
    async (err: any, decoded: any) => {
      if (err) throw err;
      const data = await postsEntity.find({
        user: decoded._id,
      });
      let searchResults = [];
      const filteredData = data.filter((post: any) => post.post[0].title.includes(search));
      response.send(ResponseFormat("", { data: filteredData }));
    }
  );
});

export { router as searchRouter };
