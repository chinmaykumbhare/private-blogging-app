import {Router, Request, Response} from "express";
import { ResponseFormat } from "../modules/response";

const router = Router();

router.get("/", async (request: Request, response: Response) => {
    response.send(ResponseFormat("", {data: "hit"}));
})

/**
 * upload profile picture
 * multer integration
 */

router.post("/pic", async (request: Request, response: Response) => {
    response.send(ResponseFormat("", {data: "hit"}));
})

export {router as profileRouter};