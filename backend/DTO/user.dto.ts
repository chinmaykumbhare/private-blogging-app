import { Document } from "mongoose";

export default interface Users extends Document {
    firstname: string,
    lastname: string,
    username: string,
    password: string,
    email: string
}