export interface MessageBody {
    err: String,
    message: Object
}