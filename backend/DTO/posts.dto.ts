import { Document, SchemaDefinitionProperty, Types } from "mongoose";

export default interface Posts extends Document {
    user: Types.ObjectId,
    image: string,
    post: SchemaDefinitionProperty<ArrayConstructor>
}