import {Request, Response, Router} from "express";
import { ResponseFormat } from "../../../modules/response";
import { userRouter } from "../../../router/userRouter";

const router = Router();

router.get("/", (request: Request, response: Response) => {
    response.send(ResponseFormat("", {data: "hit"}));
})

router.use("/user", userRouter);

export {router as v1Routes};