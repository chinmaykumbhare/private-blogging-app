import {Router} from "express";
import { v1Routes } from "./routes";

const router = Router();

router.use("/v1.0.1", v1Routes);

export {router as APIv1};