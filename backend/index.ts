import express from "express";
import {config} from "dotenv";
import { Error } from "mongoose";
import { APIv1 } from "./controller/API/v1.0.1/API";
const mongoose = require("mongoose");
const cors = require("cors");

const server = express();
config();

server.use(cors());
server.use(express.json());
server.use(express.urlencoded({extended: true}));

server.use(express.static("images"));

mongoose.connect(process.env.DB_CONNECTION_STRING, (err: Error) => {
    if(err) throw err;
    console.log("Connected to MONGO");
});

//versioning
server.use("/api", APIv1);

server.listen(8090, () => {
    console.log("Server running on port 8090");
})