import { MessageBody } from "../DTO/messageBody";

export function ResponseFormat(err: string, message: object) {
    return <MessageBody>{err: err, message: message};
}