import { Request, Response } from "express";
import { ResponseFormat } from "../modules/response";

export async function TokenValidator(request: Request, response: Response, next: () => void) {
    const {token} = request.body;
    if(!token) {
        response.send(ResponseFormat("Token Not Provided", {}));
    } else {
        next();
    }
}