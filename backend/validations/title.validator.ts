import { Request, Response } from "express";
import { ResponseFormat } from "../modules/response";

export async function TitleValidator(request: Request, response: Response, next: () => void) {
    const {title} = request.body;
    if(!title) {
        response.send(ResponseFormat("title Not Provided", {}));
    } else {
        next();
    }
}