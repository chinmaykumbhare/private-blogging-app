import { Request, Response } from "express";
import { ResponseFormat } from "../modules/response";

export async function FirstNameValidator(
  request: Request,
  response: Response,
  next: () => void
) {
  const { firstname } = request.body;
  if (!firstname) {
    response.send(ResponseFormat("firstname Not Provided", {}));
  } else {
    next();
  }
}
