import { Request, Response } from "express";
import { ResponseFormat } from "../modules/response";

export async function TagsValidator(request: Request, response: Response, next: () => void) {
    const {tags} = request.body;
    if(!tags) {
        response.send(ResponseFormat("tags Not Provided", {}));
    } else {
        next();
    }
}