import { Request, Response } from "express";
import { ResponseFormat } from "../modules/response";

export async function BodyValidator(request: Request, response: Response, next: () => void) {
    const {body} = request.body;
    if(!body) {
        response.send(ResponseFormat("body Not Provided", {}));
    } else {
        next();
    }
}