import { Request, Response } from "express";
import { ResponseFormat } from "../modules/response";

export async function EmailValidator(
  request: Request,
  response: Response,
  next: () => void
) {
  const { email } = request.body;
  if (!email) {
    response.send(ResponseFormat("email Not Provided", {}));
  } else {
    next();
  }
}
