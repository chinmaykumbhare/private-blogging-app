import { Request, Response } from "express";
import { ResponseFormat } from "../modules/response";

export async function UsernameValidator(
  request: Request,
  response: Response,
  next: () => void
) {
  const { username } = request.body;
  if (!username) {
    response.send(ResponseFormat("username Not Provided", {}));
  } else {
    next();
  }
}
