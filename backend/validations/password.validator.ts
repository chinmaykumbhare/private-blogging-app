import { Request, Response } from "express";
import { ResponseFormat } from "../modules/response";

export async function PasswordValidator(
  request: Request,
  response: Response,
  next: () => void
) {
  const { password } = request.body;
  if (!password) {
    response.send(ResponseFormat("password Not Provided", {}));
  } else {
    next();
  }
}
