import { Request, Response } from "express";
import { ResponseFormat } from "../modules/response";

export async function LastNameValidator(
  request: Request,
  response: Response,
  next: () => void
) {
  const { lastname } = request.body;
  if (!lastname) {
    response.send(ResponseFormat("lastname Not Provided", {}));
  } else {
    next();
  }
}
